/*
 * Zadanie polega na dodaniu do programu funckjonalności:
 * - edytowania wskazanego drzewa
 * - wyświetlania szczegółowych informacji na temat wskazanego drzewa
 */

import java.util.Scanner;

public class Main {
	
	/*
	 * Przykład klasy w języku Java. Klasy to nic innego jak złożone typy danych (prostymi są int, float, double, boolean; generalnie
	 * wszystkie te, które nie mają dodatkowych własności i właściwości). W naszym wypadku klasa posiada jedynie pola (czyli zmienne). 
	 * Wszystkie są typu public; public jest modyfikatorem zasięgu - w poniższym wypadku wskazuje, że utworzone zmienne w klasie 
	 * mogą być używane z dowolnej części programu, który będzie korzystał z klasy. Pozostałe modyfikatory będą pokazane w następnych przykładach.
	 * Sama klasa również posiada modyfikator public co oznacza, że może być używana w dowolnym miejscu tworzonego programu bez żadnych 
	 * ograniczeń (również w innych klasach). 
	 * 
	 * Dodatkowym modyfikatorem jest słowo static. Oznacza ono, że tworzona klasa istnieje w prgramie przez cały czas jego trwania (od uruchomienia
	 * do jego zakończenia). Jest to o  tyle ważne, że klasa utworzona została wewnątrz innej klasy (o nazwie Main), któa z kolei
	 * NIGDY nie jest inicjalizowana w programie (to ona wywołuje cały program). Bez tego modyfikatora słownego program nie mógłby wykorzystywać
	 * klasy Drzewo. 
	 * 
	 * Każda klasa w Java dziedziczy pewne podstawowe cechy po superklasie (bazowej) Object. W związku z tym nasza klasa będzie posiada pewne dodatkowe
	 * metody (funkcje klasy - omawiane w kolejnych przykładach), które nie zostały tutaj utworzone
	 */
	public static class Drzewo {
		public int korzenie;
		public String pien;
		public String kora;
		public int typ_lisci;
		public String podtyp_lisci;
		public String ukwiecenie;
		public String owoce;
		public float ilosc_lat;
		public String nazwa;
	}
	
	//na potrzeby programu utworzymy sobie tablicę obiektów o nazwie drzewa. Na razie nie nadajemy jej rozmiaru (ilości obiektów, które będzie
	//przechowywać); zrobimy to po uruchomieniu programu.
	public static Drzewo[] drzewa;
	
	public static boolean menu(Scanner s) {
		int i = 0;
		print("\n\nWitaj w programie do katalogoania drzew. Dostępne opcje:" +
				"\n1. Dodaj nowe drzewo\n2. Usuń drzewo" +
				"\n3. Pokaż informacje o drzewie\n4. Wyświetl listę drzew" +
				"\n5. Wyjście z programu" +
				"\nTwój wybór: ");
		do {
			i = s.nextInt();
		}while(i < 1 && i > 5);
		switch(i) {
			case 1: dodajDrzewo(s); break;
			case 2: usunDrzewo(s); break;
			case 3: pokazDrzewo(); break;
			case 4: pokazListe(); break;
			default: return true;
		}
		return false;
	}
	
	public static void dodajDrzewo(Scanner s) {
		//Scanner str = new Scanner(System.in);
		print("\nPodaj nazwę drzewa: ");
		//while(!s.hasNextLine()) s.next();
		int num = 0;
		for (;num<drzewa.length;num++) {
			if (drzewa[num].nazwa == null)
				break;
		}
		if (num==drzewa.length) {
			print("Obecnie masz dodany pełny zestaw drzew. Usuń jakieś" +
		"bądz edytuj któreś");
			return;
		}
		//ku przypomnieniu - bez tej linii scanner przeskoczyłby odwolanie do nabliższej metody nextLine() ponieważ przechowuje już w sobie
		//poprzedni strumień (wybór z menu), w którym to został wprowadzony znak ENTER/RETURN, co z kolei powoduje, że scanner przeszedłby do następnego wyboru
		s.nextLine();
		//odwołanie do składowych obiektu (w tym przypadku pola nazwa) odbywa się przez tzw. spójnik (kropkę). Wskazuje ona, że obiekt o nazwie
		//drzewa (w zasadzie obiekt znajdujący się w tablicy obiektów drzewa pod indeksem num) posiada właściwość (pole) nazwa, do którego chcemy przypisać
		//określoną wartość (z funkcji s.nextLine()). 
		
		//PROSZĘ ZAUWAŻYĆ, że sam Scanner (reprezentowany przez zmienna s) jest OBIEKTEM klasy Scanner!
		drzewa[num].nazwa = s.nextLine();
		print("\nPodaj rodzaj korzeni: ");
		drzewa[num].korzenie = s.nextInt();
	}
	
	public static void usunDrzewo(Scanner s) {
		print("\nPodaj numer drzewa, które chcesz usunąć[zakres 1-" +
	drzewa.length + "; domyślnie 1]: ");
		int wybor = 1;
		do {
			wybor = s.nextInt();
		} while(wybor < 1 || wybor > drzewa.length);
		drzewa[wybor-1].nazwa = null;
	}
	
	public static void pokazDrzewo() {
		
	}
	
	public static void pokazListe() {
		for (int i=0;i<drzewa.length;i++) {
			if (drzewa[i].nazwa!=null) {
				print("\nDrzewo " + (i+1) + ": " + drzewa[i].nazwa + ",");
			}
		}
	}
	
	
	
	public static void main(String[] args) {
		Scanner wejscie = new Scanner(System.in);
		//użytkownik deklaruje jakiej wielkości ma być tablica drzew; jeżeli tego nie zrobi, utworzymy tablicę z maksymalną dozwolną wielkością
		print("Witaj w programie do katalogowania drzew. Podaj wielkość"
				+ " tablicy drzew (domyślnie maksymalna dostępna wielkość):");
		
		int rozmiar = wejscie.nextInt();
		if (rozmiar != 0) {
			drzewa = new Drzewo[rozmiar];
		}
		else {
			//maksymalna dozwolona wielkość
			drzewa =  new Drzewo[Integer.MAX_VALUE];
		}
		//samo przypisanie rozmiaru tablicy to połowa sukcecu. Zmienna bazujące na klasie (czyli obiekty) MUSZĄ zostać zainicjowane 
		//przed ich wykorzystaniem w programie. W naszym zamyśle WSZYSTKIE OBIEKTY zostaną zainicjowane w pętli for zanim wykona się dalsza część programu
		for(int i =0; i<drzewa.length;i++) {
			//inicjalizacja polega na utworzeniu w pamięci (czyli rzerwacji) obszaru, w którym będzie zaposywany nasz obiekt. Dokonuje się tego
			//przez słowo new (utwórz NOWY obiekt) i wywołanie tzw. konstruktora obiektu (w tym wypadku używamy nazwy naszej klasy - tzw. 
			//konstruktor domyślny (istnieje zawsze, tworzy się tzw. niejawnie czyli bez naszej ingerencji). W następnych przykładach utworzmy
			//włąsne konstruktory
			drzewa[i] = new Drzewo();
		}
		//pętla, która działa w nieskończoność (przerywa ją wynik true na wyjściu menu)
		while(!menu(wejscie));
		
		//zamknięcie strumienia wprowadzania danych; w kolejnym kroku program kończy działanie
		wejscie.close();
	}
	
	public static void print(String s) {
		System.out.print(s);
	}
	
//PONIŻEJ PRZYKŁAD działania klas; utworzona klasa DRZEWO posiadająca pewne cechy 
//charakterystyczne dla wszystkich drzew. Następnie mamy tworzone obiekty 
//na podstawie klasy o nazwie kasztanowiec oraz jodła, które mają poszczególne
//skłądowe klasy uzupełnione o wskazane wartości
	//DRZEWO
	//korzenie
	//pień
	//kora
	//typ_lisci
	//podtyp_lisci
	//ukwiecenie
	//owoce
	//ilosc_lat
	
	
	//kasztanowiec <- DRZEWO
	//krzenie: ukorzenienie koronowe
	//pień: jednolity gruby
	//kora: gruba karbona
	//typ_lisci: lisciaste
	//podtyp_lisci: potrójny
	//ukwiecenie: białe kopułkowe
	///owoce; niejadalne orzechowe
	//ilosc_lat: 10
	
	//jodala <- DRZEWO
	//korzenie: proste
	//pień: jednolity cienki
	//kora: cienka gładka
	//typ_lisci: iglaste
	//podtyp_lisci: długie, kłujące
	//ukwiecenie: strąkowate
	//owoce: szyszki 
	//ilosc_lat 5
}
